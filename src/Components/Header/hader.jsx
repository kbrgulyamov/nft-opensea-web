import React, { useEffect, useState } from 'react'

function Header() {
      const [fixed, setFixed] = useState(false)

      useEffect(() => {
            window.addEventListener('scroll', () => {
                  if (window.scrollY > 530) {
                        setFixed(true)  // 100 fixed header 
                  } else {
                        setFixed(false)
                  }
            })
      })

      const reload = () => {
            window.location.reload()
      }

      return (
            <header className={`header ${fixed && "fixedNav"}`}>
                  <div className='container_main'>
                        <div className='left_info_header_link shit'>
                              <div className='logo'>
                                    <div className='ccirecli_logo'></div>
                                    <h1 >NFT<span style={{ color: '#671AE4' }}>.Ring</span></h1>
                              </div>

                              <div className='link_info'>
                                    <a href="#">Home</a>
                                    <a href="/active">Active</a>
                                    <a href="/feature">Feature</a>
                                    <a href="/community">Community</a>
                              </div>

                        </div>

                        <div className='right_btn btn_top'>
                              <button className='btn_header' onClick={reload}>Select Wallet</button>
                        </div>
                  </div>

            </header>
      )
}

export default Header