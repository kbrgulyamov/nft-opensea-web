import React, { useState, useEffect } from 'react'
import Aos from "aos";
import "aos/dist/aos.css"
import Header from '../Header/hader'
import Sponsors from '../NewGeneration/Sponsors'
import CardNft from '../BlockNFTCard/CardNfT'
import CardsNft from '../CardsNfts/cardsNfts'
import TitlePrice from '../NftPrice/Price'
import BlockPrice from '../NftBlockPrice/BlockPrice'
import axios from 'axios'


function MainPage(props) {
      const [nfts, setNfts] = useState([])
      const [nftsInfo, setNftsInfo] = useState([])

      // NFT FETCH 
      const options = {
            method: "GET",
            url: "https://api.opensea.io/api/v1/assets?format=json",
      };

      const fetchNfts = async () => {
            const res = await axios.request(options)

            setNfts(res.data.assets)
      }

      useEffect(() => {
            fetchNfts()
      }, [])

      useEffect(() => {
            Aos.init({ duration: 2700 })
      }, [])



      // NFT PRICE INFO
      // const price = {
      //       method: "GET",
      //       url: 'https://api.coindesk.com/v1/bpi/currentprice.json'
      // }

      // const fetcPrice = async () => {
      //       const res = await axios.request(price)

      //       setNftsInfo(res.data.rates)
      // }

      // useEffect(() => {
      //       fetcPrice()
      // }, [])



      return (
            <div className="bg_wraper">
                  <Header />
                  <div className='container_main'>
                        <div className="text_info_main" data-aos="fade-right">
                              <h1 style={{ color: '#fff' }} className='wrp-text'>The Home of<br /> Minimalist Buying<br /> and Selling</h1>
                              <p className='p_info_down'>This NFT website is a website that features<br /> buying and selling, news and blogs.<br /> Designed with a minimalist and<br /> informative concept</p>
                        </div>

                        <div className='btns_down' data-aos="fade-right">
                              <button className='btn-1'>Explore</button>
                              <button className='btn-2'>Create</button>
                        </div>

                        <div className='block_numbers' data-aos="fade-right">
                              <div className='number_1'>
                                    <h2>37k+</h2>
                                    <p>Artworks</p>
                              </div>

                              <div className='number_1'>
                                    <h2>20k+</h2>
                                    <p>Artists</p>
                              </div>

                              <div className='number_1'>
                                    <h2>99k+</h2>
                                    <p>Aucations</p>
                              </div>
                        </div>

                        <div className='right_block_img' >
                              <div className='img_nft_block_center' data-aos="fade-left">
                                    <img className='top_img' src="https://lh3.googleusercontent.com/TQwHaIASSz0r0aeCgzjRdibAQtjIC7GkdDzIGFb6gbTWyzrNBuNb8VkjkxTgYi1n3_KodjTYHeinwJAkUxdx8qmkhgS4WnPr9c23LAc" alt="" />
                                    <img className='meddle_img' src="https://lh3.googleusercontent.com/NpO1xJQDQuxWAiZeHKZvbSwbE2cUxhcx8EA0mtKW2VTQQSTZ-sxAhcG13-yuYQSOmHRk6Ml0MrWUSTdZBL9uwEMKKznxYW0-s2w8" alt="" />
                              </div>
                              <div className='img_nft_block_center' data-aos="fade-left">
                                    <img className='top_img' src="https://lh3.googleusercontent.com/dkoNh84SxlbzgEP9SeDkphvSLik8dD34iF_6Q8PYOhJYr261dpPSgV5vAHHzGTm3cQZVK1pGfnEEuVBZ3-miksqc9oUmNeeOJIr6e30" alt="" />
                                    <img className='meddle_img' src="https://lh3.googleusercontent.com/gv9Zj3tbGHqt7D3kM5QYf6C2LkqFhgk2wQ5BM5nMoS5BE5nCZY7STw2jFmf8Ez11k2MpmL_Zg2nFStIicRFk1XzByU_zFcHGu23oYVE" alt="" />
                              </div>
                              <div className='img_wrap_column' data-aos="fade-left">
                                    <img className='nft_img' src='https://lh3.googleusercontent.com/YuIZolPYbfbIda526CHg2ZqaDb08vinnuq83fG0mWkpxoeU98mEMWNqE7GXAPcEIdFalFGgFv5mfN0JIJaa28nX49RNc3ISb14ligg' alt='' />
                              </div>

                        </div>
                        <Sponsors />
                        <CardNft />
                        <div className='card_nft'>
                              {nfts.map((el) => (
                                    <CardsNft
                                          image={el.image_url}
                                          key={el.id}
                                          name={el.name}
                                          link={el.permalink}
                                    />
                              ))}
                        </div>

                        <TitlePrice />
                        <BlockPrice />
                  </div>
            </div >
      )
}

export default MainPage