import React, { useState, useEffect } from "react";
import { Transition } from 'react-transition-group';


const CardNft = (props) => {
      const [loaderVisible, setloaderVisible] = useState(false)
      return (
            <div className="wrap_nft_block">
                  <div className="title_cardNft" >
                        <h1>Colleaction</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur<br /> adipiscing elit.</p>
                  </div>

                  <div className="filter_btn">
                        <button>Art</button>
                        <button onClick={() => setloaderVisible(!loaderVisible)}>{loaderVisible ? "Loading..." : "Pottern"}</button>
                        <div className="wraper">
                              <Transition in={loaderVisible} timeout={500} mountOnEnter unmountOnExit>
                                    {state => <div className={`circli ${state}`} />}
                              </Transition>

                        </div>
                        <button >NFT ARTS</button>
                        <button>NFT RING</button>
                  </div>
            </div>
      )
}


export default CardNft