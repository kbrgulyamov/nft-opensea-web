import React from 'react'
import img_red from '../NewGeneration/img/red.jpeg'
import img_blue from '../NewGeneration/img/blue.jpeg'
import img_blue_2 from '../NewGeneration/img/blue2.jpeg'




function Sponsors(props) {
      return (
            <div className='Wrap_Block_Div' >
                  <div className='top_title_shop' data-aos="fade-up">
                        <h1>New Generation of <br /> Online Shoping</h1>
                        <p>This NFT website is a website that features buying and selling, news and<br /> blogs. Designed with a minimalist and informative concept</p>
                  </div>

                  <div className='wrap_cards_block' >
                        <div className='card_one' >
                              <div className='img_block'>
                                    <img className='card_img' src='https://s3-alpha-sig.figma.com/img/8e79/c034/dc68bdf98a800719872f2d7bf07232bf?Expires=1655078400&Signature=YLAlHtEL621KXr~9BQ4rCFEsFHj8dfk9G724mP8dq48dbqeP18tnuSSkr2oCyAj5S8rR3qZxS2wvb1PouKprS0srzJeSMyR0F1RtnFX9mfiZqAms0wd5ZeE0PPlnZ74viNXLtfNYiiX1dU4gfj8MVM2WVgEWC1PWbjYYKxyIlGmBjd4AqwHobVwTrzDDGgoT9BDFp~OJs-LhboTtu718UghYz~m7cGqDzTPY3IYmEQ6Cnu3dhplyiNwpvwbzAmi6XxIEuqHp93K4vIJv~qNmPEmmRv5mpC-dE1VvRp~UgD5hSTDAmD3NsMjMIcAKOdMM7d2fULsSqj0MAZI2u8VdQQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA' alt="" />
                              </div>
                              <div className='descriptions'>
                                    <h1>NFT Green</h1>
                              </div>

                              <div className='hover_block'>

                              </div>
                        </div>



                        <div className='card_one_2'>
                              <div className='img_block'>
                                    <img className='card_img' src={img_blue} alt="" />
                              </div>
                              <div className='descriptions'>
                                    <h1>NFT Purple Teme</h1>
                              </div>

                              <div className='hover_block_2'>

                              </div>
                        </div>



                        <div className='card_one_3'>
                              <div className='img_block'>
                                    <img className='card_img' src={img_red} alt="" />
                              </div>
                              <div className='descriptions'>
                                    <h1>NFT Red Teme</h1>
                              </div>

                              <div className='hover_block'>

                              </div>
                        </div>



                        <div className='card_one_4'>
                              <div className='img_block'>
                                    <img className='card_img' src={img_blue_2} alt="" />
                              </div>
                              <div className='descriptions'>
                                    <h1>NFT Blue</h1>
                              </div>

                              <div className='hover_block'>

                              </div>
                        </div>



                        <div className='card_one'>
                              <div className='img_block'>
                                    <img className='card_img' src={img_blue} alt="" />
                              </div>
                              <div className='descriptions'>
                                    <h1>NFT Purple</h1>
                              </div>

                              <div className='hover_block'>

                              </div>
                        </div>



                  </div>


            </div>
      )
}

export default Sponsors