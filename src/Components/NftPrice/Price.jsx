import React from 'react'

const TitlePrice = () => {
      return (
            <div className="wrap_title_nft_price">
                  <h1 className='title_price_wrap'>Featured Artworks</h1>
                  <p>This NFT website is a website that features buying and selling, news and<br /> blogs. Designed with a minimalist and informative concept</p>
            </div>
      )
}

export default TitlePrice