import React, { useEffect, useState } from 'react'
import Aos from "aos";
import "aos/dist/aos.css"

const CardsNft = ({ name, image, link }) => {
      useEffect(() => {
            Aos.init({ duration: 2700 })
      }, [])
      return (
            <div className="wrap_card_flex">
                  <div className="card_nft_block" data-aos="flip-left"
                        data-aos-easing="ease-out-cubic"
                        data-aos-duration="2000">
                        <div className="img_nft_top">
                              <img src={image} alt={"nft" + " " + name} />
                        </div>

                        <div className='price_descriptions'>
                              <div className="block_descriptions">
                                    <h1>Descriptions</h1>
                                    <h1 style={{ color: "#fff" }}>{name}</h1>
                              </div>

                              <div className="price_block">
                                    <h1 style={{ marginBottom: '6px' }}>Price</h1>
                                    <h1 style={{ color: '#fff', fontSize: "22px" }}>0.93</h1>
                              </div>
                        </div>

                        <div className="btn_link_opensie">
                              <a href={link} >
                                    <button>OPEN NFT OPENSEA</button>
                              </a>
                        </div>

                  </div>
            </div>
      )
}

export default CardsNft