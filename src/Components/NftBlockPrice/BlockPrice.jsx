import React from 'react'
import img_block from '../NftBlockPrice/img/img_1.png'
import eh from '../NftBlockPrice/img/ethereum-classic-(etc).svg'



const BlockPrice = ({ name, value, unit, type, code }) => {
      return (
            <div className='flex_block_price'>
                  <div className='card_price'>
                        <div className='img_wrap_nft'>
                              <img src={img_block} alt="" />
                        </div>
                        <div className='block_info_wrapper'>
                              <div className="_top_img_ciricli">

                                    <div className="ciricle_one">
                                          <img src="https://lh3.googleusercontent.com/gNmn9AGzmuEIzyFR-l6odpDr5c78LSLgbTekdlWKPE-pS1q0YEmE3nl7T0Kx3comJKI-xM83eHsih3Pt7OxgqG2oM3Wf-M3J6ZqELA=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_1">
                                          <img src="https://lh3.googleusercontent.com/952DR0TzVsft4vaFB7ANllREwmkBa7f4F8lH0K1BPDjVxfvkS37nIpwlHFOBXhX0Rb-GHynh3bJh8bt_-11hRxpcqWTHdIrjg46l=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_2">
                                          <img src="https://lh3.googleusercontent.com/fc1CawMCX9zrgWOsLBL1tWoBHZh2cQuqjBxHjKXW5ySdvPoyr-5DeyZylzOCgfeWijhoDnpEeIIaDpGJ49mO5sDw2pXKqPpgS0mU1w=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_3">
                                          <img src="https://lh3.googleusercontent.com/VUMGwPlitYdff6eFGxH6rzl2K-JnJoD-NiKd1SJJzoqzM11LTH8El0EETZSAHEzK1WWTHw1qDoQd6-ulcPBAePTTSkesHclT8iDFd5A=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_4">
                                          <img src="https://lh3.googleusercontent.com/VUMGwPlitYdff6eFGxH6rzl2K-JnJoD-NiKd1SJJzoqzM11LTH8El0EETZSAHEzK1WWTHw1qDoQd6-ulcPBAePTTSkesHclT8iDFd5A=w358" alt="" />
                                    </div>
                              </div>

                              <div className="wrapper_top_block">
                                    <div className='title_nft'>
                                          <h2>God of War - NFT</h2>
                                    </div>
                                    <div className="rigt_price">
                                          <img src={eh} alt="" />
                                          <h3>390eth</h3>
                                    </div>
                              </div>

                              <div className="wrapper_bottom_block">
                                    <h3>ToomuchLag</h3>
                                    <button>Purchase</button>
                              </div>

                        </div>
                  </div>

                  <div className='card_price'>
                        <div className='img_wrap_nft'>
                              <img src={img_block} alt="" />
                        </div>
                        <div className='block_info_wrapper'>
                              <div className="_top_img_ciricli">

                                    <div className="ciricle_one">
                                          <img src="https://lh3.googleusercontent.com/gNmn9AGzmuEIzyFR-l6odpDr5c78LSLgbTekdlWKPE-pS1q0YEmE3nl7T0Kx3comJKI-xM83eHsih3Pt7OxgqG2oM3Wf-M3J6ZqELA=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_1">
                                          <img src="https://lh3.googleusercontent.com/952DR0TzVsft4vaFB7ANllREwmkBa7f4F8lH0K1BPDjVxfvkS37nIpwlHFOBXhX0Rb-GHynh3bJh8bt_-11hRxpcqWTHdIrjg46l=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_2">
                                          <img src="https://lh3.googleusercontent.com/fc1CawMCX9zrgWOsLBL1tWoBHZh2cQuqjBxHjKXW5ySdvPoyr-5DeyZylzOCgfeWijhoDnpEeIIaDpGJ49mO5sDw2pXKqPpgS0mU1w=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_3">
                                          <img src="https://lh3.googleusercontent.com/VUMGwPlitYdff6eFGxH6rzl2K-JnJoD-NiKd1SJJzoqzM11LTH8El0EETZSAHEzK1WWTHw1qDoQd6-ulcPBAePTTSkesHclT8iDFd5A=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_4">
                                          <img src="https://lh3.googleusercontent.com/VUMGwPlitYdff6eFGxH6rzl2K-JnJoD-NiKd1SJJzoqzM11LTH8El0EETZSAHEzK1WWTHw1qDoQd6-ulcPBAePTTSkesHclT8iDFd5A=w358" alt="" />
                                    </div>
                              </div>

                              <div className="wrapper_top_block">
                                    <div className='title_nft'>
                                          <h2>God of War - NFT</h2>
                                    </div>
                                    <div className="rigt_price">
                                          <img src={eh} alt="" />
                                          <h3>390eth</h3>
                                    </div>
                              </div>

                              <div className="wrapper_bottom_block">
                                    <h3>ToomuchLag</h3>
                                    <button>Purchase</button>
                              </div>

                        </div>
                  </div>

                  <div className='card_price'>
                        <div className='img_wrap_nft'>
                              <img src={img_block} alt="" />
                        </div>
                        <div className='block_info_wrapper'>
                              <div className="_top_img_ciricli">

                                    <div className="ciricle_one">
                                          <img src="https://lh3.googleusercontent.com/gNmn9AGzmuEIzyFR-l6odpDr5c78LSLgbTekdlWKPE-pS1q0YEmE3nl7T0Kx3comJKI-xM83eHsih3Pt7OxgqG2oM3Wf-M3J6ZqELA=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_1">
                                          <img src="https://lh3.googleusercontent.com/952DR0TzVsft4vaFB7ANllREwmkBa7f4F8lH0K1BPDjVxfvkS37nIpwlHFOBXhX0Rb-GHynh3bJh8bt_-11hRxpcqWTHdIrjg46l=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_2">
                                          <img src="https://lh3.googleusercontent.com/fc1CawMCX9zrgWOsLBL1tWoBHZh2cQuqjBxHjKXW5ySdvPoyr-5DeyZylzOCgfeWijhoDnpEeIIaDpGJ49mO5sDw2pXKqPpgS0mU1w=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_3">
                                          <img src="https://lh3.googleusercontent.com/VUMGwPlitYdff6eFGxH6rzl2K-JnJoD-NiKd1SJJzoqzM11LTH8El0EETZSAHEzK1WWTHw1qDoQd6-ulcPBAePTTSkesHclT8iDFd5A=w358" alt="" />
                                    </div>
                                    <div className="ciricle_one left_4">
                                          <img src="https://lh3.googleusercontent.com/VUMGwPlitYdff6eFGxH6rzl2K-JnJoD-NiKd1SJJzoqzM11LTH8El0EETZSAHEzK1WWTHw1qDoQd6-ulcPBAePTTSkesHclT8iDFd5A=w358" alt="" />
                                    </div>
                              </div>

                              <div className="wrapper_top_block">
                                    <div className='title_nft'>
                                          <h2>God of War - NFT</h2>
                                    </div>
                                    <div className="rigt_price">
                                          <img src={eh} alt="" />
                                          <h3>390eth</h3>
                                    </div>
                              </div>

                              <div className="wrapper_bottom_block">
                                    <h3>ToomuchLag</h3>
                                    <button>Purchase</button>
                              </div>

                        </div>
                  </div>


            </div>
      )
}

export default BlockPrice