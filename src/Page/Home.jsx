import { useState } from 'react'
import MainPage from '../Components/Main/main'


const HomePage = () => {
      return (
            <div className='container_wrapper'>
                  <MainPage />
            </div>
      )
}

export default HomePage