import './App.css'
import HomePage from './Page/Home'

function App() {
  return (
    <div className="App">
      <HomePage />
    </div>
  )
}

export default App 